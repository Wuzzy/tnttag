local S = minetest.get_translator("tnttag")

minetest.register_node("tnttag:tnt", {
	description = S("TNTTagger - Tag other players"),
	tiles = {"tnt_top.png", "tnt_side.png", "tnt_side.png"},
	on_place = function(itemstack, placer, pointed_thing)
		local placer_name = placer:get_player_name()
		if arena_lib.is_player_in_arena(placer_name, "tnttag") then
			return nil
		else
			return minetest.item_place_node(itemstack, placer, pointed_thing)
		end
	end,
	on_drop = function(itemstack, dropper, pos)
		local dropper_name = dropper:get_player_name()
		if arena_lib.is_player_in_arena(dropper_name, "tnttag") then
			return nil
		else
			return minetest.item_drop(itemstack, dropper, pos)
		end
	end,
	on_use = function(itemstack, user, pointed_thing)
		local user_name = user:get_player_name()
		if arena_lib.is_player_in_arena(user_name, "tnttag") then
			if pointed_thing.type == "object" then
				if not pointed_thing.ref:get_luaentity() then
					local player = pointed_thing.ref
					local p_name = player:get_player_name()
					if arena_lib.is_player_in_arena(p_name, "tnttag") then
						local arena = arena_lib.get_arena_by_player(user_name)
						if not arena.players[p_name].tagged then
							tnttag.tagplayer(p_name, arena)
							tnttag.untagplayer(user_name, arena)
							-- Messages
							for pl_name, _ in pairs(arena.players) do
								minetest.chat_send_player(pl_name, S("@1 tagged @2", user_name, p_name))
					    	end
							arena_lib.HUD_send_msg("title", p_name, S("You have been tagged by @1!", user_name),1, nil--[[sound?]], 0xFF3300)
							arena_lib.HUD_send_msg("title", user_name, S("You tagged @1!", p_name),1, nil--[[sound?]], 0xFF3300)
						end
					end
				end
			end
		end
	end,
})

tnttag.tagitem = "tnttag:tnt"
