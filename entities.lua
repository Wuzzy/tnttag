minetest.register_entity("tnttag:tnt_if_tagged", {
	initial_properties = {
		collide_with_objects = false,
		pointable = false,
		collisionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
        selectionbox = {-0.25, -0.25, -0.25, 0.25, 0.25, 0.25},
		visual = "cube",
		visual_size = {x = .5, y = .5, z = .5},
		textures = {"tnt_top.png", "tnt_bottom.png", "tnt_side.png", "tnt_side.png", "tnt_side.png", "tnt_side.png"},
		--is_visible = false,
	},
	_timer = 0,
	on_step = function(self, dtime, moveresult)
		if self._timer > .3 and self.object:get_attach() == nil then
			self.object:remove()
		else
			self._timer = self._timer + dtime
		end
	end,
})
