# TNTTAG

## Create an own arena
`/tnttag create <arena> <minplayers:int> <maxplayers:int>`

## Edit own arena

### Warnings

#### Timers

Don't set timer-time it won't change anything. Set the lap_time in settings.

## Licensing

Main-License: LICENSE.txt
Media License: LICENSE_MEDIA.txt textures/license.txt

## Schems for you arenas

Find nice arenas on: https://codeberg.org/debiankaios/tnttag_arenas

## Chatcommandbuilder

Thankyou to rubenwardy for chatcommandbuilder.lua.
Here links to original:
https://github.com/rubenwardy/ChatCmdBuilder
https://gitlab.com/rubenwardy/ChatCmdBuilder