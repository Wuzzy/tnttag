arena_lib.on_load("tnttag", function(arena)
	arena.original_player_amount = arena.players_amount
	if arena.autochoosewaves then arena.waves = math.ceil((arena.players_amount - 1)/arena.max_players_in_autochoose) end
	for p_name, stats in pairs(arena.players) do
		tnttag.generate_HUD(arena, p_name)
		--[[ arena.players[p_name].tnt_if_tagged = minetest.add_entity(minetest.get_player_by_name(p_name):get_pos(), "tnttag:tnt_if_tagged", nil)
		 arena.players[p_name].tnt_if_tagged:set_attach(minetest.get_player_by_name(p_name), "Head", {x=0, y=10, z=0})]]
	end
end)

arena_lib.on_start("tnttag", function(arena)
	arena.current_time = arena.wavetime*arena.waves
	tnttag.newwave(arena)
end)

arena_lib.on_join("tnttag", function(p_name, arena, as_spectator)
	if as_spectator then
		tnttag.generate_HUD(arena, p_name)
		tnttag.update_pause_hud(arena)
	end
end)

arena_lib.on_quit("tnttag", function(arena, p_name, is_spectator, reason)
	if not is_spectator then
		tnttag.remove_tnthead(p_name)
	end
	tnttag.remove_HUD(arena, p_name)
end)

arena_lib.on_eliminate("tnttag", function(arena, p_name)
	tnttag.remove_tnthead(p_name)
end)

arena_lib.on_celebration("tnttag", function(arena, winner)
	arena.current_time = 0
	for p_name in pairs(arena.players) do
		tnttag.remove_tnthead(p_name)
	end
end)

arena_lib.on_end("tnttag", function(arena, players, winners, spectators)
	for p_name in pairs(players) do
		tnttag.remove_HUD(arena, p_name)
	end
	for p_name in pairs(spectators) do
		tnttag.remove_HUD(arena, p_name)
	end
end)

arena_lib.on_time_tick("tnttag", function(arena)
	if arena.pause then
		if arena.current_time%arena.wavetime == 0 then
			tnttag.newwave(arena)
		end
	else
		local taggersnum = 0
		for p_name,stats in pairs(arena.players) do
			if tnttag.gettagstatus(p_name, arena) then taggersnum = taggersnum + 1 end
		end
		if taggersnum == 0 then
			arena.current_time=(arena.current_time-arena.current_time%arena.wavetime)-1
			tnttag.newwave(arena)
		end
		if arena.current_time%arena.wavetime == 0 then
			for p_name in pairs(arena.players) do
	        	if arena.players[p_name].tagged then
					tnttag.explode_player(p_name, arena)
				end
	    	end
			arena.current_time = arena.current_time + arena.pause_length
			if arena.pause_length == 0 then
				tnttag.newwave(arena)
			else
				arena.pause = true
				tnttag.update_pause_hud(arena)
			end
		end
	end
	tnttag.update_wave_timer_hud(arena)
	tnttag.update_player_count_hud(arena)
end)

arena_lib.on_timeout("tnttag", function(arena)
	winners = {}
	for p_name in pairs(arena.players) do
		if arena.players[p_name].tagged then
			tnttag.explode_player(p_name, arena)
		else
			table.insert(winners, p_name)
		end
	end
	if (#winners > 1) then
		arena_lib.load_celebration("tnttag", arena, table.concat(winners,", "))
	end
end)
